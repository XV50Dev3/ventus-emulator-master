INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 10, 5763, 0, 7, 0, 32, 302, 30), /* Orc Moss Potion */
(0, 8, 2410, 0, 7, 0, 32, 302, 30), /* Jade */
(0, 16, 2410, 0, 7, 0, 32, 302, 15), /* Jade */
(0, 4, 2411, 0, 7, 0, 32, 302, 30), /* Loa Runic Powder */
(0, 8, 2411, 0, 7, 0, 32, 302, 15), /* Loa Runic Powder */
(0, 4, 2416, 0, 7, 0, 32, 302, 30), /* Moritius Obsidian */
(0, 8, 2416, 0, 7, 0, 32, 302, 15), /* Moritius Obsidian */
(0, 1, 2462, 0, 7, 0, 32, 302, 15), /* Belial's Runestone */
(0, 1, 2432, 0, 7, 0, 32, 302, 15), /* Broken Magic Sword of Belial */
(0, 1, 1026, 0, 7, 0, 32, 302, 15), /* Cellon ( level 10) */
(0, 1, 4447, 0, 7, 0, 32, 302, 15), /* Orc Warchief's Sword */
(0, 1, 4448, 0, 7, 0, 32, 302, 15), /* Lion Loa Sword */
(0, 1, 4450, 0, 7, 0, 32, 302, 15), /* Orc Sniper's Bow */
(0, 1, 4451, 0, 7, 0, 32, 302, 15), /* Eagle Loa Bow */
(0, 1, 4453, 0, 7, 0, 32, 302, 15), /* Warlock's Divine Staff */
(0, 1, 4454, 0, 7, 0, 32, 302, 15), /* Snake Loa Staff */
(0, 1, 4456, 0, 7, 0, 32, 302, 15), /* Orc Sekraz Gauntlets */
(0, 1, 4457, 0, 7, 0, 32, 302, 15), /* Bear Loa Gauntlets */
(0, 1, 4460, 0, 7, 0, 32, 302, 15), /* Lion Loa Crossbow */
(0, 1, 4467, 0, 7, 0, 32, 302, 15), /* Lion Loa Dagger */
(0, 1, 4470, 0, 7, 0, 32, 302, 15), /* Snake Loa Spell Gun */
(0, 1, 4473, 0, 7, 0, 32, 302, 15), /* Blessed Sekraz Jade Token */
(0, 1, 4476, 0, 7, 0, 32, 302, 15), /* Lion Loa Sekraz Stone Armour */
(0, 1, 4479, 0, 7, 0, 32, 302, 15), /* Eagle Loa Leather Armour */
(0, 1, 4482, 0, 7, 0, 32, 302, 15), /* Snake Loa Battle Gear  */
(0, 1, 4505, 0, 7, 0, 32, 302, 15), /* Bear Loa Light Armour */
(0, 1, 4521, 0, 7, 0, 32, 302, 15), /* Occult Necklace */
(0, 1, 4517, 0, 7, 0, 32, 302, 15), /* Jade Spirit Ring */
(0, 1, 4513, 0, 7, 0, 32, 302, 15), /* Snake Loa Bracelet */
(0, 1, 5751, 0, 7, 0, 32, 302, 15), /* Orc Loa Weapon Production Scroll */
(0, 1, 5750, 0, 7, 0, 32, 302, 15), /* Orc Loa Accessories Production Scroll */
(0, 1, 5752, 0, 7, 0, 32, 302, 15), /* Orc Loa Secondary Weapon Production Scroll */
(0, 1, 5753, 0, 7, 0, 32, 302, 15); /* Orc Loa Armour Production Scroll */
