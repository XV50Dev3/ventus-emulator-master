INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4810, 0, 7, 0, 21, 302, 15), /* Hongbi's Specialist Partner Card */
(0, 1, 4811, 0, 7, 0, 21, 302, 15), /* Cheongbi's Specialist Card  */
(0, 1, 2418, 0, 7, 0, 21, 302, 30), /* Mysterious Hair Dye */
(0, 1, 2282, 0, 7, 0, 21, 302, 30), /* Angel's Feather */
(0, 1, 9041, 0, 7, 0, 21, 302, 30), /* Ancelloan Blessing */
(0, 1, 547, 0, 7, 0, 21, 302, 30), /* Ayam (7 Days) */
(0, 1, 733, 0, 7, 0, 21, 302, 30), /* Rose Hanbok (7 Days) */
(0, 1, 492, 0, 7, 0, 21, 302, 30), /* Blue Founder Hat (7 Days) */
(0, 1, 671, 0, 7, 0, 21, 302, 30), /* Summer Hanbok (7 Days) */
(0, 1, 2514, 0, 7, 0, 21, 302, 15), /* Small Ruby of Completion */
(0, 1, 2515, 0, 7, 0, 21, 302, 15), /* Small Sapphire of Completion */
(0, 1, 2516, 0, 7, 0, 21, 302, 15), /* Small Obsidian of Completion */
(0, 1, 2517, 0, 7, 0, 21, 302, 15), /* Small Topaz of Completion */
(0, 1, 2518, 0, 7, 0, 21, 302, 15), /* Ruby of Completion */
(0, 1, 2519, 0, 7, 0, 21, 302, 15), /* Sapphire of Completion */
(0, 1, 2520, 0, 7, 0, 21, 302, 15), /* Obsidian of Completion */
(0, 1, 2521, 0, 7, 0, 21, 302, 15), /* Topaz of Completion */
(0, 1, 9376, 0, 7, 0, 21, 302, 15); /* Evil Twin */