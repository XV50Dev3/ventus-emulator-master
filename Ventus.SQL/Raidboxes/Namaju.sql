INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 334, 0, 7, 0, 6, 302, 10), /* Sealed Princess Sakura Bead */
(0, 1, 227, 0, 7, 0, 6, 302, 30), /* X Mask */
(0, 1, 8003, 0, 7, 0, 6, 302, 30), /* Amulet of Attack and Defence Levels */
(0, 1, 1078, 0, 7, 0, 6, 302, 30), /* Specialist Point Potion */
(0, 3, 1092, 0, 7, 0, 6, 302, 30), /* Group Time-Space Piece */
(0, 3, 1095, 0, 7, 0, 6, 302, 30), /* Hunting Time-Space Piece */
(0, 50, 1012, 0, 7, 0, 6, 302, 30), /* Seed of Power */
(0, 1, 9363, 0, 7, 0, 6, 302, 15); /* Sakura's Hero */
 