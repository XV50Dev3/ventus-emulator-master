INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES

(0, 1, 4132, 0, 0, 0, 99, 185, 10), /* Varik */
(0, 1, 9330, 0, 0, 0, 99, 185, 5), /* The Chosen One Title */
(0, 50, 1244, 0, 0, 0, 99, 185, 100), /* Divine Recovery Potion */
(0, 1, 5816, 0, 0, 0, 99, 185, 2), /* Ice Witch Costume */
(0, 1, 2439, 0, 0, 0, 99, 185, 30); /* Turik's Essence */

