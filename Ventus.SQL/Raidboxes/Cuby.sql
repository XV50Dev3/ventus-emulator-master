INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 315, 0, 7, 0, 0, 302, 10), /* Flame Gloves */
(0, 1, 321, 0, 7, 0, 0, 302, 30), /* Sparkle Shoes */
(0, 1, 262, 0, 7, 0, 0, 302, 15), /* Elvin's Sword */
(0, 1, 265, 0, 7, 0, 0, 302, 15), /* Red Bow */
(0, 3, 268, 0, 7, 0, 0, 302, 15), /* Red Sage Wand */
(0, 3, 26, 0, 7, 0, 0, 302, 30), /* Golden Sabre */
(0, 50, 40, 0, 7, 0, 0, 302, 30), /* Leather Bow */
(0, 1, 54, 0, 7, 0, 0, 302, 30), /* Mana Wand */
(0, 1, 9357, 0, 7, 0, 0, 302, 15); /* Sweet Tooth */
