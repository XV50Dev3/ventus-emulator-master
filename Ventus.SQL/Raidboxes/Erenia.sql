INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4958, 0, 7, 0, 24, 302, 15), /* Sealed Heavenly Sword */
(0, 1, 4960, 0, 7, 0, 24, 302, 15), /* Sealed Heavenly Bow */
(0, 1, 4959, 0, 7, 0, 24, 302, 15), /* Sealed Heavenly Staff */
(0, 1, 4955, 0, 7, 0, 24, 302, 15), /* Sealed Heavnely Crossbow */
(0, 1, 4957, 0, 7, 0, 24, 302, 15), /* Sealed Heavenly Dagger */
(0, 1, 4956, 0, 7, 0, 24, 302, 15), /* Sealed Heavenly Spell Gun */
(0, 1, 4932, 0, 7, 0, 24, 302, 15), /* Sealed Heavy Heavenly Armour */
(0, 1, 4951, 0, 7, 0, 24, 302, 15), /* Sealed Heavenly Leather Armour */
(0, 1, 4950, 0, 7, 0, 24, 302, 15), /* Sealed Heavenly Robe */
(0, 1, 4941, 0, 7, 0, 24, 302, 15), /* Sealed Heavenly Helmet */
(0, 1, 4971, 0, 7, 0, 24, 302, 15), /* Sealed Hevenly Leather Hat */
(0, 1, 4973, 0, 7, 0, 24, 302, 15), /* Sealed Heavenly Headband */
(0, 1, 4976, 0, 7, 0, 24, 302, 15), /* Zenas' Luxury High Heels */
(0, 1, 4967, 0, 7, 0, 24, 302, 15), /* Sealed Heavenly Gloves */
(0, 1, 4968, 0, 7, 0, 24, 302, 15), /* Sealed Heavenly Shoes */
(0, 1, 1685, 0, 7, 0, 24, 302, 30), /* Angel Wings */
(0, 1, 9095, 0, 7, 0, 24, 302, 15), /* Archangel Wings */
(0, 3, 2814, 0, 7, 0, 24, 302, 30), /* Silk */
(0, 3, 2805, 0, 7, 0, 24, 302, 30), /* Crystal of Balance */
(0, 3, 2812, 0, 7, 0, 24, 302, 30), /* Intact Orichalcum */
(0, 3, 2517, 0, 7, 0, 24, 302, 30), /* Small Topaz of Completion */
(0, 3, 2521, 0, 7, 0, 24, 302, 30), /* Topaz of Completion */
(0, 2, 2513, 0, 7, 0, 24, 302, 30), /* Dragon Heart */
(0, 3, 2285, 0, 7, 0, 24, 302, 30), /* Shining Blue Soul */
(0, 10, 2282, 0, 7, 0, 24, 302, 30), /* Angel's Feather */
(0, 1, 5886, 0, 7, 0, 24, 302, 15), /* Ancelloan's Secondary Weapon Production Scroll */
(0, 1, 5887, 0, 7, 0, 24, 302, 15), /* Ancelloan's Armour Production Scroll */
(0, 1, 1025, 0, 7, 0, 24, 302, 15), /* Cellon (level 9) */
(0, 1, 2427, 0, 7, 0, 24, 302, 15), /* Zenas' Egg */
(0, 1, 4755, 0, 7, 0, 24, 302, 15), /* Zenas' Divin Ring */
(0, 1, 4767, 0, 7, 0, 24, 302, 15), /* Magic Blue Dragon Token */
(0, 1, 9378, 0, 7, 0, 24, 302, 15); /* Little Angel */
