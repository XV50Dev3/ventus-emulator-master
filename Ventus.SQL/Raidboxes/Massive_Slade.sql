INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 317, 0, 7, 0, 4, 302, 30), /* Divine Gloves */
(0, 1, 322, 0, 7, 0, 4, 302, 30), /* Shadow Shoes */
(0, 1, 264, 0, 7, 0, 4, 302, 15), /* Broken Elemental Sword */
(0, 1, 267, 0, 7, 0, 4, 302, 15), /* Majestic Bow */
(0, 1, 270, 0, 7, 0, 4, 302, 15), /* Majestic wand */
(0, 1, 299, 0, 7, 0, 4, 302, 15), /* Flamberge */
(0, 1, 300, 0, 7, 0, 4, 302, 15), /* Piercing Force Bow */
(0, 1, 301, 0, 7, 0, 4, 302, 15), /* Kai's Wand */
(0, 1, 9361, 0, 7, 0, 4, 302, 15); /* Rock Solid */
