INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 5141, 0, 7, 0, 12, 302, 30), /* Piece of the Pirate SP (Event) */
(0, 1, 5144, 0, 7, 0, 12, 302, 30), /* O'Peng's Treasure Map */
(0, 5, 5145, 0, 7, 0, 12, 302, 30), /* Lime Juice (Event) */
(0, 1, 5146, 0, 7, 0, 12, 302, 30), /* Pirate Flag Signpost */
(0, 1, 4166, 0, 7, 0, 12, 302, 15), /* Leona */
(0, 1, 4165, 0, 7, 0, 12, 302, 15), /* Mini Kangpen */
(0, 1, 4188, 0, 7, 0, 12, 302, 15), /* Navy Bushtail */
(0, 1, 4189, 0, 7, 0, 12, 302, 15), /* Pirate Bushi (Water) */
(0, 1, 4190, 0, 7, 0, 12, 302, 15), /* Pirate Bushi (Fire) */
(0, 1, 4191, 0, 7, 0, 12, 302, 15), /* Pirate Bushi (Light) */
(0, 1, 4192, 0, 7, 0, 12, 302, 15), /* Pirate Bushi (Shadow) */
(0, 1, 4099, 0, 7, 0, 12, 302, 30), /* Pirate Specialist Card */
(0, 1, 8185, 0, 7, 0, 12, 302, 30), /* Navy Hat (1 Day) */
(0, 1, 8188, 0, 7, 0, 12, 302, 30), /* Navy Hat (Permanent) */
(0, 1, 8189, 0, 7, 0, 12, 302, 30), /* Navy Costume (1 Day) */
(0, 1, 8192, 0, 7, 0, 12, 302, 30), /* Navy Costume (Permanent) */
(0, 1, 9368, 0, 7, 0, 12, 302, 15); /* Arrrrr! */