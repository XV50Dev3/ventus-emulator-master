INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4918, 0, 7, 0, 14, 302, 15), /* Flame Giant Shell Armour */
(0, 1, 4921, 0, 7, 0, 14, 302, 15), /* Blaze Hound's Leather Armour */
(0, 1, 4924, 0, 7, 0, 14, 302, 15), /* Fiery Phoenix's Robe */
(0, 1, 4920, 0, 7, 0, 14, 302, 15), /* Forgotten Hero's Chest Armour */
(0, 1, 4923, 0, 7, 0, 14, 302, 15), /* Forgotten Hero's Leather Armour */
(0, 1, 4926, 0, 7, 0, 14, 302, 15), /* Forgotten Hero's Robe */
(0, 1, 4932, 0, 7, 0, 14, 302, 15), /* Valakus' Gloves */
(0, 1, 4730, 0, 7, 0, 14, 302, 15), /* Ladine's Tear */
(0, 1, 4748, 0, 7, 0, 14, 302, 15), /* Akamur Armour */
(0, 1, 5900, 0, 7, 0, 14, 302, 30), /* Charred Mask Parchment */
(0, 1, 5901, 0, 7, 0, 14, 302, 30), /* Grenigas Accessories Parchment */
(0, 1, 2502, 0, 7, 0, 14, 302, 30), /* Grenigas Armour Parchment */
(0, 1, 2509, 0, 7, 0, 14, 302, 15), /* Katol's Pattern */
(0, 1, 2507, 0, 7, 0, 14, 302, 30), /* Ancient Civilisation Tablet */
(0, 1, 2506, 0, 7, 0, 14, 302, 30), /* Blazing Piece of Red Metal */
(0, 3, 2503, 0, 7, 0, 14, 302, 30), /* Earth Element */
(0, 1, 2504, 0, 7, 0, 14, 302, 30), /* Spring Water */
(0, 7, 2900, 0, 7, 0, 14, 302, 30), /* Broken Diamond */
(0, 2, 2901, 0, 7, 0, 14, 302, 30), /* Intact Water */
(0, 5, 2901, 0, 7, 0, 14, 302, 30), /* Intact Diamond */
(0, 10, 2901, 0, 7, 0, 14, 302, 30), /* Inatct Diamond */
(0, 1, 5918, 0, 7, 0, 14, 302, 15), /* Right Side of Grenigas' Raid seal */
(0, 1, 9370, 0, 7, 0, 14, 302, 15); /* Pyromaniac */
