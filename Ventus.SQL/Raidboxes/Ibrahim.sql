INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 1876, 0, 7, 0, 9, 302, 15), /* Ibrahim's Golden Key */
(0, 1, 1872, 0, 7, 0, 9, 302, 30), /* Robber Gang Gold Coin */
(0, 1, 2349, 0, 7, 0, 9, 302, 30), /* Shiny Sky Blue Gemstone */
(0, 1, 9366, 0, 7, 0, 9, 302, 15); /* Master Thief */
