INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 9350, 0, 7, 0, 28, 302, 15), /* Test Subject */
(0, 1, 8310, 0, 7, 0, 28, 302, 15), /* Mad Professor Macavity's Test Subject */
(0, 1, 8311, 0, 7, 0, 28, 302, 15), /* Mad Professor Macavity */
(0, 1, 4080, 0, 7, 0, 28, 302, 15), /* Special Ops Bushi */
(0, 1, 4125, 0, 7, 0, 28, 302, 30), /* Shogun Bushi */
(0, 1, 4126, 0, 7, 0, 28, 302, 30), /* Centurio Bushi */
(0, 1, 943, 0, 7, 0, 28, 302, 30), /* Boxer Bushi */
(0, 1, 4063, 0, 7, 0, 28, 302, 30), /* Rusty Robby */
(0, 1, 980, 0, 7, 0, 28, 302, 30), /* Bull Bushtail */
(0, 1, 2158, 0, 7, 0, 28, 302, 30), /* Gourmet Pet Food */
(0, 1, 2325, 0, 7, 0, 28, 302, 30), /* Strange Pet Food */
(0, 1, 2418, 0, 7, 0, 28, 302, 30), /* Mysterious Hair Dye */
(0, 1, 4340, 0, 7, 0, 28, 302, 30), /* Magic Speed Booster (1 Day) */
(0, 1, 2396, 0, 7, 0, 28, 302, 30), /* Green Gem */
(0, 1, 1945, 0, 7, 0, 28, 302, 15), /* Sealed Vessel */
(0, 1, 4344, 0, 7, 0, 28, 302, 15), /* White Witch Laurena's Disguise */
(0, 1, 4173, 0, 7, 0, 28, 302, 30), /* Spooky Tarot Card [Ghost] */
(0, 1, 4174, 0, 7, 0, 28, 302, 30), /* Spooky Tarot Card [Dracula] */
(0, 1, 4175, 0, 7, 0, 28, 302, 30), /* Spooky Tarot Card [Scarecrow] */
(0, 1, 1918, 0, 7, 0, 28, 302, 30); /* Halloween Signpost */


