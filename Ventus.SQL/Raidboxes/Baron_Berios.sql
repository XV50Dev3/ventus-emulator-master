INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES

(0, 1, 4131, 0, 0, 0, 99, 999, 10), /* Rumial */
(0, 1, 9330, 0, 0, 0, 99, 999, 5), /* The Chosen One Title */
(0, 50, 1244, 0, 0, 0, 99, 999, 100), /* Divine Recovery Potion */
(0, 1, 5816, 0, 0, 0, 99, 999, 2), /* Ice Witch Costume */
(0, 1, 2438, 0, 0, 0, 99, 999, 30); /* Eperial's Essence */

