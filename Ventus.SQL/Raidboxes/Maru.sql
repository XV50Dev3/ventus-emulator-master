INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 8231, 0, 7, 0, 19, 302, 15), /* Maru's Specialist Partner Card */
(0, 1, 4809, 0, 7, 0, 19, 302, 15), /* Maru in Mother's Fur (PSP)*/
(0, 1, 4819, 0, 7, 0, 19, 302, 15), /* Jinn's Specialist Partner Card */
(0, 1, 1218, 0, 7, 0, 19, 302, 15), /* Equipment Protection */
(0, 1, 1219, 0, 7, 0, 19, 302, 15), /* Release Scroll */
(0, 1, 2520, 0, 7, 0, 19, 302, 15), /* Obsidian of Completion */
(0, 1, 2159, 0, 7, 0, 19, 302, 15), /* Partner Medicine */
(0, 1, 8046, 0, 7, 0, 19, 302, 30), /* Asian Black Bear Costume (7 Days) */
(0, 1, 8050, 0, 7, 0, 19, 302, 30), /* Asian Black Bear Hat (7 Days) */
(0, 1, 8047, 0, 7, 0, 19, 302, 30), /* Polar Bear Costume (7 Days) */
(0, 1, 8051, 0, 7, 0, 19, 302, 30), /* Polar Bear Hat (7 Days) */
(0, 1, 8049, 0, 7, 0, 19, 302, 30), /* Teddy Bear Costume (7 Days) */
(0, 1, 8052, 0, 7, 0, 19, 302, 30), /* Teddy Bear Hat (7 Days) */
(0, 1, 8208, 0, 7, 0, 19, 302, 30), /* Cuddly Tiger Hat (7 Days)*/
(0, 1, 8212, 0, 7, 0, 19, 302, 30), /* Snow White Tiger Hat (7 Days) */
(0, 1, 8200, 0, 7, 0, 19, 302, 30), /* Cuddly Tiger Costume(7 Days) */
(0, 1, 8204, 0, 7, 0, 19, 302, 30), /* Snow White Tiger Costume (7 Days) */
(0, 1, 446, 0, 7, 0, 19, 302, 30), /* Panda Hat (7 Days) */
(0, 1, 2158, 0, 7, 0, 19, 302, 30), /* Gourmet Pet Food */
(0, 1, 2325, 0, 7, 0, 19, 302, 30), /* Strange Pet Food */
(0, 1, 9374, 0, 7, 0, 19, 302, 15); /* Doppelgänger */