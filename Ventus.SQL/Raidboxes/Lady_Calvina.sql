INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES

(0, 1, 4130, 0, 0, 0, 99, 942, 10), /* Ladine */
(0, 1, 9330, 0, 0, 0, 99, 942, 5), /* The Chosen One Title */
(0, 50, 1244, 0, 0, 0, 99, 942, 100), /* Divine Recovery Potion */
(0, 1, 5816, 0, 0, 0, 99, 942, 2), /* Ice Witch Costume */
(0, 1, 2437, 0, 0, 0, 99, 942, 30); /* Woondine's Essence */

