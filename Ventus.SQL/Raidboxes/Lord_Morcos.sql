INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES

(0, 1, 4129, 0, 0, 0, 99, 882, 10), /* Elkaim */
(0, 1, 9330, 0, 0, 0, 99, 882, 5), /* The Chosen One Title */
(0, 50, 1244, 0, 0, 0, 99, 882, 100), /* Divine Recovery Potion */
(0, 1, 5816, 0, 0, 0, 99, 882, 2), /* Ice Witch Costume */
(0, 1, 2436, 0, 0, 0, 99, 882, 30); /* Sellaim's Essence */


