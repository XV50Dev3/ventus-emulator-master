INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 316, 0, 7, 0, 3, 302, 30), /* Storm Gloves */
(0, 1, 319, 0, 7, 0, 3, 302, 30), /* Blaze Shoes */
(0, 1, 141, 0, 7, 0, 3, 302, 15), /* Slayer */
(0, 1, 148, 0, 7, 0, 3, 302, 15), /* Siege Bow */
(0, 1, 155, 0, 7, 0, 3, 302, 15), /* Soul Wand */
(0, 1, 292, 0, 7, 0, 3, 302, 15), /* Balenty Crossbow */
(0, 1, 290, 0, 7, 0, 3, 302, 15), /* Kris */
(0, 1, 294, 0, 7, 0, 3, 302, 15), /* Ray Spell Gun */
(0, 1, 298, 0, 7, 0, 3, 302, 15), /* Splendid Defender */
(0, 1, 296, 0, 7, 0, 3, 302, 15), /* Robe of Light */
(0, 1, 272, 0, 7, 0, 3, 302, 15), /* Trial Robe */
(0, 1, 9360, 0, 7, 0, 3, 302, 15); /* Arachnophobe */