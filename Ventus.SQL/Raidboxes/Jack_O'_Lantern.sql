INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 950, 0, 7, 0, 10, 302, 15), /* Pumpkin Bushtail */
(0, 1, 4173, 0, 7, 0, 10, 302, 30), /* Spooky Tarot Card [Ghost] */
(0, 1, 4174, 0, 7, 0, 10, 302, 30), /* Spooky Tarot Card [Dracula] */
(0, 1, 4175, 0, 7, 0, 10, 302, 30), /* Spooky Tarot Card [Scarecrow]*/
(0, 1, 4169, 0, 7, 0, 10, 302, 30), /* Glowing Pumpkin Hat (1 Day) */
(0, 1, 4170, 0, 7, 0, 10, 302, 30), /* Glowing Pumpkin Hat (7 Days) */
(0, 1, 4171, 0, 7, 0, 10, 302, 15), /* Glowing Pumpkin Hat (30 Days) */
(0, 1, 4172, 0, 7, 0, 10, 302, 15), /* Glowing Pumpkin Hat (Permanent) */
(0, 1, 2281, 0, 7, 0, 10, 302, 30), /* Pumpkin */
(0, 1, 1918, 0, 7, 0, 10, 302, 30), /* Halloween Signspot */
(0, 1, 9348, 0, 7, 0, 10, 302, 15); /* Horror */