INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES

(0, 1, 5876, 0, 0, 0, 0, 5958, 10), /*  Fairy Egg (Erenia) */
(0, 1, 5875, 0, 0, 0, 0, 5958, 10), /* Fairy Egg (Zenas) */
(0, 1, 9100, 0, 0, 0, 0, 5958, 8), /* Onyx Wings (Limited) */
(0, 1, 4490, 0, 0, 0, 0, 5958, 4), /* Caligor's Horn */
(0, 50, 1244, 0, 0, 0, 0, 5958, 100), /* Divine Recovery Potion */
(0, 1, 9326, 0, 0, 0, 0, 5958, 20), /* Living Legend Title */
(0, 1, 4129, 0, 0, 0, 0, 5958, 10), /* Elkaim */
(0, 1, 4130, 0, 0, 0, 0, 5958, 10), /* Ladine */
(0, 1, 4131, 0, 0, 0, 0, 5958, 10), /* Rumial */
(0, 1, 4132, 0, 0, 0, 0, 5958, 10); /* Varik */


