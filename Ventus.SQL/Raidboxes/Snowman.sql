INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4800, 0, 7, 0, 8, 302, 15), /* Aegir's Specialist Partner Card */
(0, 1, 4802, 0, 7, 0, 8, 302, 15), /* Barni's Specialist Partner Card */
(0, 1, 4803, 0, 7, 0, 8, 302, 15), /* Freya's Specialist Partner Card */
(0, 1, 4804, 0, 7, 0, 8, 302, 15), /* Shinobi's Specialist Partner Card */
(0, 1, 4805, 0, 7, 0, 8, 302, 15), /* Lotus' Specialist Partner Card */
(0, 1, 4806, 0, 7, 0, 8, 302, 15), /* Orkani's Specialist Partner Card */
(0, 1, 5932, 0, 7, 0, 8, 302, 30), /* Partner Skill Ticket (All) */
(0, 1, 8091, 0, 7, 0, 8, 302, 30), /* Christmas Hat (7 Days) */
(0, 1, 8092, 0, 7, 0, 8, 302, 30), /* Christmas Hat (30 Days) */
(0, 1, 4076, 0, 7, 0, 8, 302, 15), /* Christmas Hat (Permanent) */
(0, 1, 8094, 0, 7, 0, 8, 302, 30), /* Christmas Costume (7 Days) */
(0, 1, 8095, 0, 7, 0, 8, 302, 30), /* Christmas Costume (30 Days) */
(0, 1, 4075, 0, 7, 0, 8, 302, 15), /* Christmas Costume (Permanent) */
(0, 1, 8116, 0, 7, 0, 8, 302, 30), /* Santa Bushtail Costume (1 Day) */
(0, 1, 8117, 0, 7, 0, 8, 302, 30), /* Santa Bushtail Hat (1 Day) */
(0, 1, 8118, 0, 7, 0, 8, 302, 30), /* Santa Bushtail Costume (7 Days) */
(0, 1, 8119, 0, 7, 0, 8, 302, 30), /* Santa Bushtail Hat (7 Days) */
(0, 1, 8120, 0, 7, 0, 8, 302, 15), /* Santa Bushtail Costume (30 Days) */
(0, 1, 8121, 0, 7, 0, 8, 302, 15), /* Santa Bushtail Hat(30 Days) */
(0, 1, 1369, 0, 7, 0, 8, 302, 30), /* Jingle Bell */
(0, 1, 5207, 0, 7, 0, 8, 302, 30), /* Pyjama Upgarde Scroll */
(0, 1, 2328, 0, 7, 0, 8, 302, 30), /* Rudolph's Pet Trainer */
(0, 1, 445, 0, 7, 0, 8, 302, 30), /* Snowman */
(0, 1, 5970, 0, 7, 0, 8, 302, 30), /* Maru's Raid Seal */
(0, 1, 5213, 0, 7, 0, 8, 302, 15), /* Christmas Costume Box  */
(0, 1, 3113, 0, 7, 0, 8, 302, 30), /* Christmas Tree */
(0, 1, 3114, 0, 7, 0, 8, 302, 30), /* Christmas Decoration */
(0, 1, 1581, 0, 7, 0, 8, 302, 30), /* Christmas Toy */
(0, 1, 4406, 0, 7, 0, 8, 302, 15), /* Red-nosed Reindeer Bead */
(0, 1, 9365, 0, 7, 0, 8, 302, 15); /* Sleepy Head */