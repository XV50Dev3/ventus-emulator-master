INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4981, 0, 7, 0, 25, 302, 15), /* Broken Krem's Right Blade */
(0, 1, 4982, 0, 7, 0, 25, 302, 15), /* Seli-Lusha's Branche of Light */
(0, 1, 4840, 0, 7, 0, 25, 302, 15), /* Azrael's Mask */
(0, 1, 4958, 0, 7, 0, 25, 302, 15), /* Sealed Heavenly Sword */
(0, 1, 4960, 0, 7, 0, 25, 302, 15), /* Sealed Heavenly Bow */
(0, 1, 4959, 0, 7, 0, 25, 302, 15), /* Sealed Heavenly Staff */
(0, 1, 4955, 0, 7, 0, 25, 302, 15), /* Sealed Heavnely Crossbow */
(0, 1, 4957, 0, 7, 0, 25, 302, 15), /* Sealed Heavenly Dagger */
(0, 1, 4956, 0, 7, 0, 25, 302, 15), /* Sealed Heavenly Spell Gun */
(0, 1, 4932, 0, 7, 0, 25, 302, 15), /* Sealed Heavy Heavenly Armour */
(0, 1, 4951, 0, 7, 0, 25, 302, 15), /* Sealed Heavenly Leather Armour */
(0, 1, 4950, 0, 7, 0, 25, 302, 15), /* Sealed Heavenly Robe */
(0, 1, 2431, 0, 7, 0, 25, 302, 15), /* Fernon's Egg */
(0, 1, 1685, 0, 7, 0, 25, 302, 30), /* Angel Wings */
(0, 1, 9095, 0, 7, 0, 25, 302, 15), /* Archangel Wings */
(0, 1, 1025, 0, 7, 0, 25, 302, 15), /* Cellon (level 9) */
(0, 1, 2816, 0, 7, 0, 25, 302, 30), /* Unidentified Meta */
(0, 1, 2814, 0, 7, 0, 25, 302, 30), /* Silk */
(0, 1, 2811, 0, 7, 0, 25, 302, 30), /* Damaged Orichalcum */
(0, 1, 2805, 0, 7, 0, 25, 302, 30), /* Crystal of Balance */
(0, 1, 2819, 0, 7, 0, 25, 302, 30), /* Golden Thread */
(0, 1, 4736, 0, 7, 0, 25, 302, 15), /* Divin Fist */
(0, 1, 9380, 0, 7, 0, 25, 302, 15), /* Ancelloan's Herald */
(0, 1, 4964, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Sword */
(0, 1, 4966, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Bow */
(0, 1, 4965, 0, 7, 0, 25, 302, 15), /* Sealed HHellord Staff */
(0, 1, 4961, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Crossbow */
(0, 1, 4963, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Dagger */
(0, 1, 4962, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Spell Gun */
(0, 1, 4952, 0, 7, 0, 25, 302, 15), /* Sealed Heavy Hellord Armour */
(0, 1, 4954, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Leather Armour */
(0, 1, 4953, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Robe */
(0, 1, 4945, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Helmet */
(0, 1, 4972, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Leather Hat */
(0, 1, 4974, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Headband */
(0, 1, 4975, 0, 7, 0, 25, 302, 15), /* Erenia's Crafted Horn */
(0, 1, 4969, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Gloves */
(0, 1, 4970, 0, 7, 0, 25, 302, 15), /* Sealed Hellord Shoes */
(0, 1, 1686, 0, 7, 0, 25, 302, 30), /* Devil Wings */
(0, 1, 9096, 0, 7, 0, 25, 302, 15), /* Archdaemon Wings */
(0, 1, 2814, 0, 7, 0, 25, 302, 30), /* Silk */
(0, 1, 2805, 0, 7, 0, 25, 302, 30), /* Crystal of Balance */
(0, 1, 4839, 0, 7, 0, 25, 302, 15), /* Fernon's Shoes */
(0, 1, 4980, 0, 7, 0, 25, 302, 15), /* Broken Krem's Left Blade */
(0, 1, 4983, 0, 7, 0, 25, 302, 15), /* Azrael's Wings */
(0, 1, 4754, 0, 7, 0, 25, 302, 15); /* Champion's Armour */


