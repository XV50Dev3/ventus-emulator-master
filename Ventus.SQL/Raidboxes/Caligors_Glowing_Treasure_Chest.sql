INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES

(0, 1, 5876, 0, 0, 0, 0, 5960, 20), /*  Fairy Egg (Erenia) */
(0, 1, 5875, 0, 0, 0, 0, 5960, 20), /* Fairy Egg (Zenas) */
(0, 1, 9100, 0, 0, 0, 0, 5960, 16), /* Onyx Wings (Limited) */
(0, 1, 4490, 0, 0, 0, 0, 5960, 8), /* Caligor's Horn */
(0, 50, 1244, 0, 0, 0, 0, 5960, 80), /* Divine Recovery Potion */
(0, 1, 9326, 0, 0, 0, 0, 5960, 30), /* Living Legend Title */
(0, 1, 4129, 0, 0, 0, 0, 5960, 15), /* Elkaim */
(0, 1, 4130, 0, 0, 0, 0, 5960, 15), /* Ladine */
(0, 1, 4131, 0, 0, 0, 0, 5960, 15), /* Rumial */
(0, 1, 4132, 0, 0, 0, 0, 5960, 15); /* Varik */

