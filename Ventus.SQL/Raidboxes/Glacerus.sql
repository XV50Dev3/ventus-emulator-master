INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4497, 0, 7, 0, 17, 302, 15), /* Battle Monk Specialist Card */
(0, 1, 4498, 0, 7, 0, 17, 302, 15), /* Scout Specialist Card */
(0, 1, 4499, 0, 7, 0, 17, 302, 15), /* Tide Lord Specilaist Card */
(0, 1, 4485, 0, 7, 0, 17, 302, 15), /* Mystic Arts Specialist Card */
(0, 1, 2513, 0, 7, 0, 17, 302, 30), /* Dragon Heart */
(0, 6, 2282, 0, 7, 0, 17, 302, 30), /* Angel's Feather */
(0, 3, 1030, 0, 7, 0, 17, 302, 30), /* Full Moon Crystal */
(0, 1, 1249, 0, 7, 0, 17, 302, 30), /* Experience Potion*/
(0, 2, 2349, 0, 7, 0, 17, 302, 30), /* Shiny Sky Blue Gemstone */
(0, 3, 2349, 0, 7, 0, 17, 302, 30), /* Shiny Sky Blue Gemstone */
(0, 6, 2515, 0, 7, 0, 17, 302, 15), /* Small Sapphire of Completion */
(0, 3, 2517, 0, 7, 0, 17, 302, 15), /* Small Topaz of Completion */
(0, 1, 2519, 0, 7, 0, 17, 302, 15), /* Sapphire of Completion */
(0, 1, 9373, 0, 7, 0, 17, 302, 15); /* Ice Cold */
