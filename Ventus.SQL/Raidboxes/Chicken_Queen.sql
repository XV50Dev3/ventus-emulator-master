INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 5259, 0, 7, 0, 11, 302, 30), /* Chicken Queen's Feather (Event) */
(0, 1, 2404, 0, 7, 0, 11, 302, 30), /* Evolution Fruit (Event) */
(0, 1, 5262, 0, 7, 0, 11, 302, 30), /* Growth Fruit (Event) */
(0, 1, 5107, 0, 7, 0, 11, 302, 30), /* Chicekn Costume Upgrade Scroll (Event)*/
(0, 2, 5108, 0, 7, 0, 11, 302, 30), /* Easter Signspot */
(0, 1, 4133, 0, 7, 0, 11, 302, 30), /* Mysterious Egg) */
(0, 1, 9367, 0, 7, 0, 11, 302, 15); /* BBQ King */