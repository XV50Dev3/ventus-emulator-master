INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4489, 0, 7, 0, 20, 302, 15), /* Renegade Pecialist Cardd */
(0, 1, 4488, 0, 7, 0, 20, 302, 15), /* Avenging Angel Specialist Card */
(0, 1, 4487, 0, 7, 0, 20, 302, 15), /* Archmage Specialist Card */
(0, 1, 4813, 0, 7, 0, 20, 302, 15), /* Laurena's Specialist Partner Card */
(0, 1, 4812, 0, 7, 0, 20, 302, 15), /* Archangel Lucifer's Specialist Partner Card */
(0, 1, 4699, 0, 7, 0, 20, 302, 15), /* Laurena's Witch Hat */
(0, 5, 2282, 0, 7, 0, 20, 302, 30), /* Angel's Feather */
(0, 1, 2285, 0, 7, 0, 20, 302, 30), /* Shining Blue Soul */
(0, 1, 2397, 0, 7, 0, 20, 302, 30), /* Yellow Gem */
(0, 1, 2396, 0, 7, 0, 20, 302, 30), /* Green Gem */
(0, 1, 2514, 0, 7, 0, 20, 302, 30), /* Small Ruby of Completion */
(0, 1, 2515, 0, 7, 0, 20, 302, 30), /* Small Sapphire of Completion */
(0, 1, 2516, 0, 7, 0, 20, 302, 30), /* Small Obsidian of Completion */
(0, 1, 2518, 0, 7, 0, 20, 302, 30), /* Ruby of Completion */
(0, 1, 2519, 0, 7, 0, 20, 302, 30), /* Sapphire of Completion */
(0, 1, 2520, 0, 7, 0, 20, 302, 30), /* Obsidian of Completion */
(0, 1, 2517, 0, 7, 0, 20, 302, 30), /* Small Topaz of Completion */
(0, 1, 2521, 0, 7, 0, 20, 302, 30), /* Topaz of Completion */
(0, 1, 2434, 0, 7, 0, 20, 302, 30), /* Twilight Essence */
(0, 1, 4835, 0, 7, 0, 20, 302, 15), /* Laurena's Necklace */
(0, 1, 4836, 0, 7, 0, 20, 302, 15), /* Laurena's Ring */
(0, 1, 4837, 0, 7, 0, 20, 302, 15), /* Laurena's Bracelet */
(0, 1, 9375, 0, 7, 0, 20, 302, 15); /* Little Witch */