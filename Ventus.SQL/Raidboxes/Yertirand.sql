INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4012, 0, 7, 0, 27, 302, 15), /* Robber Gang's Golden Armour */
(0, 1, 4015, 0, 7, 0, 27, 302, 15), /* Robber Gang's Golden Tunic */
(0, 3, 4018, 0, 7, 0, 27 302, 15), /* Robber Gang's Golden Robe */
(0, 1, 4000, 0, 7, 0, 27, 302, 15), /* Desert Robbers' Longsword */
(0, 1, 4002, 0, 7, 0, 27, 302, 15), /* Desert Robbers' Bow */
(0, 1, 4004, 0, 7, 0, 27, 302, 15), /* Desert Robbers' Walking Stick */
(0, 1, 4327, 0, 7, 0, 27, 302, 15), /* Yertirand's Wand */
(0, 1, 4328, 0, 7, 0, 27, 302, 15), /* Twisted Yertirand's Wand */
(0, 10, 2282, 0, 7, 0, 27, 302, 30), /* Angel's Feather */
(0, 1, 2513, 0, 7, 0, 27, 302, 30), /* Dragon Heart */
(0, 1, 2395, 0, 7, 0, 27, 302, 30), /* Blue Gem */
(0, 1, 4308, 0, 7, 0, 27, 302, 30), /* Old Viking Costume (7 Days) */
(0, 1, 8281, 0, 7, 0, 27, 302, 15), /* Frigg's Key */
(0, 1, 8282, 0, 7, 0, 27, 302, 15), /* Ragnar's Key */
(0, 1, 8283, 0, 7, 0, 27, 302, 15), /* Erdimien's Key */
(0, 1, 8291, 0, 7, 0, 27, 302, 15), /* Jennifer's Key */
(0, 1, 8301, 0, 7, 0, 27, 302, 15), /* Yertirand's Key */
(0, 1, 4326, 0, 7, 0, 27, 302, 15), /* Bone Warrior Ragnar's Specialist Partner Crad */
(0, 1, 5997, 0, 7, 0, 27, 302, 15), /* Magic Bone Drake */
(0, 1, 9382, 0, 7, 0, 27, 302, 15); /* Twisted */
