INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4417, 0, 7, 0, 29, 302, 15), /* Mad March Hare Specialist Partner Card */
(0, 1, 4426, 0, 7, 0, 29, 302, 30), /* Easter Bunny Hat (3 Days)  */
(0, 1, 4427, 0, 7, 0, 29, 302, 30), /* Easter Bunny Hat (7 Days) */
(0, 1, 4430, 0, 7, 0, 29, 302, 30), /* Easter Bunny Costume (3 Days) */
(0, 1, 4431, 0, 7, 0, 29, 302, 30), /* Easter Bunny Costume (7 Days) */
(0, 10, 2334, 0, 7, 0, 29, 302, 30), /* Rotten Egg (Event) */
(0, 10, 2335, 0, 7, 0, 29, 302, 30), /* Cleansed Egg (Event) */
(0, 10, 2340, 0, 7, 0, 29, 302, 30), /* Easter Cake Slice (Event) */
(0, 1, 5731, 0, 7, 0, 29, 302, 30), /* Egg-Shaped Chest */
(0, 10, 2282, 0, 7, 0, 29, 302, 30), /* Angel's Feather */
(0, 1, 2514, 0, 7, 0, 29, 302, 15), /* Small Ruby of Completion */
(0, 1, 2515, 0, 7, 0, 29, 302, 15), /* Small Sapphire of Completion */
(0, 1, 2516, 0, 7, 0, 29, 302, 15), /* Small Obsidian of Completion */
(0, 1, 2517, 0, 7, 0, 29, 302, 15), /* Small Topaz of Completion */
(0, 1, 2518, 0, 7, 0, 29, 302, 15), /* Ruby of Completion */
(0, 1, 2519, 0, 7, 0, 29, 302, 15), /* Sapphire of Completion */
(0, 1, 2520, 0, 7, 0, 29, 302, 15), /* Obsidian of Completion */
(0, 1, 2521, 0, 7, 0, 29, 302, 15), /* Topaz of Completion */
(0, 1, 2328, 0, 7, 0, 29, 302, 30), /* Rudolph's Pet Trainer */
(0, 1, 9383, 0, 7, 0, 29, 302, 15); /* Mad Hatter */