INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 291, 0, 7, 0, 1, 302, 15), /* Winslet Crossbow */
(0, 1, 289, 0, 7, 0, 1, 302, 15), /* Scrammer */
(0, 1, 293, 0, 7, 0, 1, 302, 15), /* Plaz Spell Gun */
(0, 1, 297, 0, 7, 0, 1, 302, 15), /* Brave Defender */
(0, 1, 295, 0, 7, 0, 1, 302, 15), /* Sentinel Look */
(0, 1, 271, 0, 7, 0, 1, 302, 15), /* Blue Robe of the Wise */
(0, 1, 310, 0, 7, 0, 1, 302, 30), /* Ancient Crystal Ring */
(0, 1, 309, 0, 7, 0, 1, 302, 30), /* Ancient Crystal Neckles */
(0, 1, 311, 0, 7, 0, 1, 302, 30), /* Ancient Crystal Bracelet */
(0, 1, 9358, 0, 7, 0, 1, 302, 30); /* Gardener */
