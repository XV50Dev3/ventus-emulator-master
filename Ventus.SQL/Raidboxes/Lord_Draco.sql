INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4500, 0, 7, 0, 16, 302, 15), /* Gladiator Specialist Card */
(0, 1, 4501, 0, 7, 0, 16, 302, 15), /* Fire Cannoneer Specialist Card */
(0, 1, 4502, 0, 7, 0, 16, 302, 15), /* Vulcano Specilaist Card */
(0, 1, 4486, 0, 7, 0, 16, 302, 15), /* Draconic Fist Specialist Card */
(0, 3, 2511, 0, 7, 0, 16, 302, 30), /* Dragon Skin */
(0, 2, 2512, 0, 7, 0, 16 302, 30), /* Dragon Blood */
(0, 1, 2513, 0, 7, 0, 16 302, 30), /* Dragon Heart */
(0, 1, 2514, 0, 7, 0, 16 302, 30), /* Small Ruby of Completion*/
(0, 1, 2516, 0, 7, 0, 16 302, 30), /* Small Obsidian of Completion */
(0, 1, 2518, 0, 7, 0, 16 302, 30), /* Ruby of Completion */
(0, 6, 2282, 0, 7, 0, 16, 302, 30), /* Angel's Feather */
(0, 3, 1030, 0, 7, 0, 16, 302, 30), /* Full Moon Crystal */
(0, 1, 1249, 0, 7, 0, 16, 302, 15), /* Experience Potion */
(0, 1, 9372, 0, 7, 0, 16 302, 15); /* Dragonslayer */
