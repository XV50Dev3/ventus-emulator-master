INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4900, 0, 7, 0, 15, 302, 15), /* Magmoros' Fire Sword */
(0, 1, 4903, 0, 7, 0, 15, 302, 15), /* Phoenix Wings */
(0, 1, 4906, 0, 7, 0, 15, 302, 15), /* Lava Ghost's Wand */
(0, 1, 4902, 0, 7, 0, 15, 302, 15), /* Forgotten Hero's Sword */
(0, 1, 4905, 0, 7, 0, 15, 302, 15), /* Forgotten Hero's Bow */
(0, 1, 4908, 0, 7, 0, 15, 302, 15), /* Forgotten Hero's Magic Wand */
(0, 1, 4930, 0, 7, 0, 15, 302, 15), /* Flame Giant Helmet */
(0, 1, 4731, 0, 7, 0, 15, 302, 15), /* Fenris' Claw */
(0, 1, 4732, 0, 7, 0, 15, 302, 15), /* Fire Golem Gauntlet */
(0, 1, 4749, 0, 7, 0, 15, 302, 15), /* Mysterious Traveller's Armour */
(0, 1, 4750, 0, 7, 0, 15, 302, 15), /* Felice's Armour */
(0, 1, 5900, 0, 7, 0, 15, 302, 30), /* Charred Mask Parchment */
(0, 1, 5901, 0, 7, 0, 15, 302, 30), /* Grenigas Accessories parchment */
(0, 1, 2500, 0, 7, 0, 15, 302, 30), /* Grenigas Weapons Parchment */
(0, 1, 2510, 0, 7, 0, 15, 302, 15), /* Grail's Pattern */
(0, 1, 2507, 0, 7, 0, 15, 302, 30), /* Ancient Civilisation Tablet */
(0, 1, 2506, 0, 7, 0, 15, 302, 30), /* Blazing Piece of Red Metal */
(0, 3, 2503, 0, 7, 0, 15, 302, 30), /* Earth Element */
(0, 1, 2504, 0, 7, 0, 15, 302, 30), /* Spring Water */
(0, 7, 2900, 0, 7, 0, 15, 302, 30), /* Broken diamond */
(0, 2, 2901, 0, 7, 0, 15, 302, 30), /* Inatct Diamond */
(0, 5, 2901, 0, 7, 0, 15, 302, 30), /* Intact Diamond */
(0, 10, 2901, 0, 7, 0, 15, 302, 30), /* Intact Diamond */
(0, 1, 9371, 0, 7, 0, 15, 302, 15); /* Phoenix */