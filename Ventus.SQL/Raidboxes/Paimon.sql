INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 1026, 0, 7, 0, 33, 302, 15), /* Cellon ( level 10) */
(0, 10, 2406, 0, 7, 0, 33, 302, 30), /* Golem Core */
(0, 2, 2430, 0, 7, 0, 33, 302, 15), /* Black Titanium Bar */
(0, 10, 2483, 0, 7, 0, 33, 302, 15), /* Paimon's Shadow Silver */
(0, 1, 2482, 0, 7, 0, 33, 302, 15), /* Paimon Soul Silver */
(0, 30, 5763, 0, 7, 0, 33, 302, 20), /* Orc Moss Potion */
(0, 1, 5750, 0, 7, 0, 33, 302, 30), /* Orc Loa Accessories Production Scroll */
(0, 1, 5751, 0, 7, 0, 33, 302, 30), /* Orc Loa Weapon Production Scroll */
(0, 1, 5752, 0, 7, 0, 33, 302, 30), /* Orc Loa Secondary Weapon Production Scroll */
(0, 1, 5753, 0, 7, 0, 33, 302, 30), /* Orc Loa Armour Production Scroll */
(0, 1, 4476, 0, 7, 0, 33, 302, 15), /* Lion Loa Sekraz Stone Armour */
(0, 1, 4479, 0, 7, 0, 33, 302, 15), /* Eagle Loa Leather Armour */
(0, 1, 4482, 0, 7, 0, 33, 302, 15), /* Snake Loa Battle Gear  */
(0, 1, 4505, 0, 7, 0, 33, 302, 15), /* Bear Loa Light Armour */
(0, 1, 4448, 0, 7, 0, 33, 302, 15), /* Lion Loa Sword */
(0, 1, 4451, 0, 7, 0, 33, 302, 15), /* Eagle Loa Bow */
(0, 1, 4454, 0, 7, 0, 33, 302, 15), /* Snake Loa Staff */
(0, 1, 4457, 0, 7, 0, 33, 302, 15), /* Bear Loa Gauntlets */
(0, 1, 4460, 0, 7, 0, 33, 302, 15), /* Lion Loa Crossbow */
(0, 1, 4467, 0, 7, 0, 33, 302, 15), /* Lion Loa Dagger */
(0, 1, 4470, 0, 7, 0, 33, 302, 15), /* Snake Loa Spell Gun  */
(0, 1, 4473, 0, 7, 0, 33, 302, 15), /* Blessed Sekraz Jade Token */
(0, 1, 4521, 0, 7, 0, 33, 302, 15), /* Occult Necklace */
(0, 1, 4517, 0, 7, 0, 33, 302, 15), /* Jade Spirit Ring */
(0, 1, 4513, 0, 7, 0, 33, 302, 15); /* Snake Loa Bracelet */