INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 348, 0, 7, 0, 7, 302, 15), /* Grasslin Fairy */
(0, 1, 2328, 0, 7, 0, 7, 302, 30), /* Rudolph's Pet Trainer  */
(0, 10, 2172, 0, 7, 0, 7, 302, 30), /* Slade's Claw */
(0, 3, 2312, 0, 7, 0, 7, 302, 30), /* Wing of Friendship (Event) */
(0, 1, 1232, 0, 7, 0, 7, 302, 30), /* Fragrant Bag */
(0, 2, 2092, 0, 7, 0, 7, 302, 30), /* Spider King's Eye */
(0, 1, 1271, 0, 7, 0, 7, 302, 30), /* Production Coupon */
(0, 3, 2173, 0, 7, 0, 7, 302, 30), /* Speaker */
(0, 10, 1004, 0, 7, 0, 7, 302, 30), /* Large Health Potion */
(0, 3, 1010, 0, 7, 0, 7, 302, 30), /* Large Recovery Potion */
(0, 10, 1231, 0, 7, 0, 7, 302, 30), /* Bamboo */
(0, 1, 2514, 0, 7, 0, 7, 302, 15), /* Small Ruby of Completion */
(0, 1, 2515, 0, 7, 0, 7, 302, 15), /* Small Sapphire of Completion */
(0, 1, 2516, 0, 7, 0, 7, 302, 15), /* Small Obsidian of Completion */
(0, 1, 2517, 0, 7, 0, 7, 302, 15), /* Small Topaz of Completion */
(0, 1, 2518, 0, 7, 0, 7, 302, 15), /* Ruby of Completion */
(0, 1, 2519, 0, 7, 0, 7, 302, 15), /* Sapphire of Completion */
(0, 1, 2520, 0, 7, 0, 7, 302, 15), /* Obsidian of Completion */
(0, 1, 2521, 0, 7, 0, 7, 302, 15), /* Topaz of Completion */
(0, 1, 9364, 0, 7, 0, 7, 302, 15); /* Gorged */