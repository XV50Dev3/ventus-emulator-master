INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 312, 0, 7, 0, 2, 302, 30), /* Ancient Diamond Neckles */
(0, 1, 313, 0, 7, 0, 2, 302, 30), /* Ancient Diamond Ring  */
(0, 1, 314, 0, 7, 0, 2, 302, 30), /* Ancient Diamond Bracelet */
(0, 1, 263, 0, 7, 0, 2, 302, 15), /* Glorious Sabre */
(0, 1, 266, 0, 7, 0, 2, 302, 15), /* Bow of Peace */
(0, 1, 269, 0, 7, 0, 2, 302, 15), /* Ghost Whisper Wand */
(0, 1, 227, 0, 7, 0, 2, 302, 30), /* X Mask */
(0, 1, 318, 0, 7, 0, 2, 302, 30), /* Death Gloves */
(0, 1, 320, 0, 7, 0, 2, 302, 30), /* Wave Shoes */
(0, 1, 9359, 0, 7, 0, 2, 302, 15); /* Ritualist */