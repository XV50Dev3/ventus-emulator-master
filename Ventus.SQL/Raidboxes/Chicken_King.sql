INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 907, 0, 7, 0, 5, 302, 15), /* Chicken Specialist Card */
(0, 20, 2181, 0, 7, 0, 5, 302, 30), /* Magic Egg */
(0, 50, 2027, 0, 7, 0, 5, 302, 30), /* Fried Chicken */
(0, 1, 254, 0, 7, 0, 5, 302, 30), /* Little Chick */
(0, 1, 255, 0, 7, 0, 5, 302, 30), /* Dancing Chick */
(0, 1, 256, 0, 7, 0, 5, 302, 30), /* Grumbly Chick */
(0, 1, 9362, 0, 7, 0, 5, 302, 15); /* Nugget */
