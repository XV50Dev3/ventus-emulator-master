INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4507, 0, 7, 0, 31, 302, 15), /* Ancient Beast Helmet */
(0, 1, 4508, 0, 7, 0, 31, 302, 15), /* Ceremonial Helmet */
(0, 3, 4509, 0, 7, 0, 31, 302, 15), /* Ancient Beast Gloves */
(0, 1, 4511, 0, 7, 0, 31, 302, 15), /* Ancient Beast Shoes */
(0, 10, 5763, 0, 7, 0, 31, 302, 30), /* Orc Moss Potion */
(0, 10, 2466, 0, 7, 0, 31, 302, 30), /* Pikanya Feather */
(0, 8, 2410, 0, 7, 0, 31, 302, 30), /* Jade */
(0, 5, 2464, 0, 7, 0, 31, 302, 30), /* Owlbear Mane */
(0, 5, 2459, 0, 7, 0, 31, 302, 30), /* Moritius Beast Leather */
(0, 4, 2411, 0, 7, 0, 31, 302, 30), /* Loa Runic Powder */
(0, 4, 2416, 0, 7, 0, 31, 302, 30), /* Moritius Obsidian */
(0, 1, 2413, 0, 7, 0, 31, 302, 15), /* Beast King Horn Fragment */
(0, 1, 1026, 0, 7, 0, 31, 302, 15), /* Cellon ( level 10) */
(0, 1, 4447, 0, 7, 0, 31, 302, 15), /* Orc Warchief's Sword */
(0, 1, 4450, 0, 7, 0, 31, 302, 15), /* Orc Sniper's Bow */
(0, 1, 4453, 0, 7, 0, 31, 302, 15), /* Warlock's Divine Staff */
(0, 1, 4456, 0, 7, 0, 31, 302, 15), /* Orc Sekraz Gauntlets */
(0, 1, 4459, 0, 7, 0, 31, 302, 15), /* Orc Warchief's Crossbow */
(0, 1, 4466, 0, 7, 0, 31, 302, 15), /* Orc Assassin's Dagger */
(0, 1, 4469, 0, 7, 0, 31, 302, 15), /* Orc Spell Gun  */
(0, 1, 4472, 0, 7, 0, 31, 302, 15), /* Sekraz Jade Token */
(0, 1, 4475, 0, 7, 0, 31, 302, 15), /* Sekraz Stone Armour */
(0, 3, 4478, 0, 7, 0, 31, 302, 15), /* Orc Hunter Leather Armour*/
(0, 1, 4481, 0, 7, 0, 31, 302, 15), /* Orc Mage Battle Gear */
(0, 1, 4484, 0, 7, 0, 31, 302, 15), /* Light Sekraz Armour */
(0, 1, 5752, 0, 7, 0, 31, 302, 30), /* Orc Loa Secondary Weapon Production Scroll */
(0, 1, 5753, 0, 7, 0, 31, 302, 30); /* Orc Loa Armour Production Scroll */


