INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 2048, 0, 7, 0, 26, 302, 30), /* High Quality Leather */
(0, 1, 2044, 0, 7, 0, 26, 302, 30), /* High Quality Rag */
(0, 3, 2604, 0, 7, 0, 26, 302, 30), /* Pure Essence of Fire */
(0, 1, 2605, 0, 7, 0, 26, 302, 15), /* Fafnir's Energy */
(0, 1, 2606, 0, 7, 0, 26, 302, 15), /* Fafnir's Horn */
(0, 1, 2607, 0, 7, 0, 26, 302, 15), /* Fafnir's Heart*/
(0, 1, 2608, 0, 7, 0, 26, 302, 15), /* Fafnir's Molar */
(0, 1, 2609, 0, 7, 0, 26, 302, 15), /* Fafnir's Scale */
(0, 1, 2200, 0, 7, 0, 26, 302, 30), /* Needle */
(0, 1, 2208, 0, 7, 0, 26, 302, 30), /* Bobbin */
(0, 1, 2511, 0, 7, 0, 26, 302, 15), /* Dragon Skin */
(0, 1, 2512, 0, 7, 0, 26, 302, 15), /* Dragon Blood */
(0, 1, 2513, 0, 7, 0, 26, 302, 15), /* Dragon Heart */
(0, 1, 5903, 0, 7, 0, 26, 302, 30), /* Fafnir's Bugle Bleuprints */
(0, 1, 4129, 0, 7, 0, 26, 302, 15), /* Elkaim */
(0, 1, 4130, 0, 7, 0, 26, 302, 15), /* Ladine */
(0, 1, 4131, 0, 7, 0, 26, 302, 15), /* Rumial */
(0, 1, 4132, 0, 7, 0, 26, 302, 15), /* Varik */
(0, 1, 4307, 0, 7, 0, 26, 302, 15), /* Dragon Wing Decoration */
(0, 1, 2611, 0, 7, 0, 26, 302, 15), /* Fafnir's Bugle */
(0, 10, 1011, 0, 7, 0, 26, 302, 30), /* Huge Recovery Potion */
(0, 1, 9381, 0, 7, 0, 26, 302, 15), /* Treasure Hunter */
