INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4909, 0, 7, 0, 13, 302, 30), /* Phoenix's Claw */
(0, 1, 4912, 0, 7, 0, 13, 302, 30), /* Blaze Hound's Fang */
(0, 1, 4915, 0, 7, 0, 13, 302, 30), /* Valaket's Breath */
(0, 1, 4911, 0, 7, 0, 13, 302, 15), /* Forgotten Hero's Crossbow */
(0, 1, 4914, 0, 7, 0, 13, 302, 15), /* Forgotten Hero's Dagger */
(0, 1, 4917, 0, 7, 0, 13, 302, 15), /* Forgotten Hero's Spell Gun */
(0, 1, 4747, 0, 7, 0, 13, 302, 15), /* Desert Robbers' Armour */
(0, 1, 4764, 0, 7, 0, 13, 302, 15), /* One-Horse Gold Token */
(0, 1, 4765, 0, 7, 0, 13, 302, 15), /* Two-Horse Gold Token */
(0, 1, 4729, 0, 7, 0, 13, 302, 15), /* Cold Metal Gauntlet */
(0, 1, 4934, 0, 7, 0, 13, 302, 15), /* Kertos' Boots */
(0, 1, 5900, 0, 7, 0, 13, 302, 30), /* Charred Mask Parchment */
(0, 1, 5901, 0, 7, 0, 13, 302, 30), /* Grenigas Accessories Parchment */
(0, 1, 2501, 0, 7, 0, 13, 302, 30), /* Grenigas Secondary Weapons Parchment */
(0, 1, 2508, 0, 7, 0, 13, 302, 30), /* Kertos' Pattern */
(0, 1, 2507, 0, 7, 0, 13, 302, 30), /* Ancient Civilisation Tablet */
(0, 1, 2506, 0, 7, 0, 13, 302, 15), /* Blazing Piece of Red Metal */
(0, 3, 2503, 0, 7, 0, 13, 302, 30), /* Earth Element */
(0, 1, 2504, 0, 7, 0, 13, 302, 30), /* Spring Water */
(0, 7, 2900, 0, 7, 0, 13, 302, 30), /* Broken Diamond */
(0, 2, 2901, 0, 7, 0, 13, 302, 30), /* Inatct Diamond */
(0, 5, 2901, 0, 7, 0, 13, 302, 30), /* Inatct Diamond */
(0, 10, 2901, 0, 7, 0, 13, 302, 30), /* Inatct Diamond */
(0, 1, 5917, 0, 7, 0, 13, 302, 15), /* Left Side of Grenigas' Raid Seal */
(0, 1, 9369, 0, 7, 0, 13, 302, 15); /* Fire Hound */
