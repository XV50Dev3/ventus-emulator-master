INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 950, 0, 7, 0, 18, 302, 15), /* Pumpkin Bushtail */
(0, 1, 4173, 0, 7, 0, 18, 302, 30), /* Spooky Tarot Card [Ghost] */
(0, 1, 4174, 0, 7, 0, 18, 302, 30), /* Spooky Tarot Card [Dracula] */
(0, 1, 4175, 0, 7, 0, 18, 302, 30), /* Spooky Tarot Card [Scarecrow]*/
(0, 1, 4169, 0, 7, 0, 18, 302, 30), /* Glowing Pumpkin Hat (1 Day) */
(0, 1, 4170, 0, 7, 0, 18, 302, 30), /* Glowing Pumpkin Hat (7 Days) */
(0, 1, 4171, 0, 7, 0, 18, 302, 30), /* Glowing Pumpkin Hat (30 Days) */
(0, 1, 4172, 0, 7, 0, 18, 302, 15), /* Glowing Pumpkin Hat (Permanent) */
(0, 1, 1004, 0, 7, 0, 18, 302, 30), /* Large Health Potion */
(0, 1, 1007, 0, 7, 0, 18, 302, 30), /* Large Mana Potion */
(0, 1, 1010, 0, 7, 0, 18, 302, 30), /* Large Recovery Potion */
(0, 1, 1918, 0, 7, 0, 18, 302, 30), /* Halloween Signspot */
(0, 1, 4807, 0, 7, 0, 18, 302, 15), /* Foxy's Specialist Partner Card */
(0, 1, 4818, 0, 7, 0, 18, 302, 15), /* Fiona's Specialist Partner Card */
(0, 1, 9349, 0, 7, 0, 18, 302, 15), /* Sly Dog */
(0, 1, 2520, 0, 7, 0, 18, 302, 15); /* Obsidian of Completion */