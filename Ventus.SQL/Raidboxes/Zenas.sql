INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
	
)
VALUES
(0, 1, 4964, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Sword */
(0, 1, 4966, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Bow */
(0, 1, 4965, 0, 7, 0, 23, 302, 15), /* Sealed HHellord Staff */
(0, 1, 4961, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Crossbow */
(0, 1, 4963, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Dagger */
(0, 1, 4962, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Spell Gun */
(0, 1, 4952, 0, 7, 0, 23, 302, 15), /* Sealed Heavy Hellord Armour */
(0, 1, 4954, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Leather Armour */
(0, 1, 4953, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Robe */
(0, 1, 4945, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Helmet */
(0, 1, 4972, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Leather Hat */
(0, 1, 4974, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Headband */
(0, 1, 4975, 0, 7, 0, 23, 302, 15), /* Erenia's Crafted Horn */
(0, 1, 4969, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Gloves */
(0, 1, 4970, 0, 7, 0, 23, 302, 15), /* Sealed Hellord Shoes */
(0, 1, 1686, 0, 7, 0, 23, 302, 30), /* Devil Wings */
(0, 1, 9096, 0, 7, 0, 23, 302, 15), /* Archdaemon Wings */
(0, 3, 2814, 0, 7, 0, 23, 302, 30), /* Silk */
(0, 3, 2805, 0, 7, 0, 23, 302, 30), /* Crystal of Balance */
(0, 3, 2812, 0, 7, 0, 23, 302, 30), /* Intact Orichalcum */
(0, 3, 2516, 0, 7, 0, 23, 302, 15), /* Small Obsidian of Completion */
(0, 3, 2520, 0, 7, 0, 23, 302, 15), /* Obsidian of Completion */
(0, 2, 2513, 0, 7, 0, 23, 302, 30), /* Dragon Heart */
(0, 3, 2285, 0, 7, 0, 23, 302, 30), /* Shining Blue Soul */
(0, 10, 2282, 0, 7, 0, 23, 302, 30), /* Angel's Feather */
(0, 1, 5885, 0, 7, 0, 23, 302, 15), /* Ancelloan's Weapon Production Scroll */
(0, 1, 5884, 0, 7, 0, 23, 302, 15), /* Ancelloan's Accessory Production Scroll */
(0, 1, 1025, 0, 7, 0, 23, 302, 15), /* Cellon (level 9) */
(0, 1, 2429, 0, 7, 0, 23, 302, 15), /* Erenia's Egg */
(0, 1, 4770, 0, 7, 0, 23, 302, 15), /* Magic Black Turtle Token */
(0, 1, 9379, 0, 7, 0, 23, 302, 15); /* Little Devil */
