﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fish : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FishInfo",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FishVNum = c.Short(nullable: false),
                        Probability = c.Short(nullable: false),
                        MapId1 = c.Short(nullable: false),
                        MapId2 = c.Short(nullable: false),
                        MapId3 = c.Short(nullable: false),
                        MinFishLength = c.Single(nullable: false),
                        MaxFishLength = c.Single(nullable: false),
                        IsFish = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FishingLog",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CharacterId = c.Long(nullable: false),
                        FishId = c.Short(nullable: false),
                        FishCount = c.Int(nullable: false),
                        MaxLength = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FishingSpots",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        MapId = c.Short(nullable: false),
                        MapX = c.Short(nullable: false),
                        MapY = c.Short(nullable: false),
                        Direction = c.Short(nullable: false),
                        MinLevel = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FishingSpots");
            DropTable("dbo.FishingLog");
            DropTable("dbo.FishInfo");
        }
    }
}
