﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class title2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CharacterTitles", "CharacterId", "dbo.Character");
            DropIndex("dbo.CharacterTitles", new[] { "CharacterId" });
            CreateTable(
                "dbo.CharacterTitle",
                c => new
                    {
                        CharacterTitleId = c.Long(nullable: false, identity: true),
                        CharacterId = c.Long(nullable: false),
                        TitleVnum = c.Long(nullable: false),
                        Stat = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.CharacterTitleId)
                .ForeignKey("dbo.Character", t => t.CharacterId, cascadeDelete: true)
                .Index(t => t.CharacterId);
            
            DropColumn("dbo.CharacterSkill", "IsPartnerSkill");
            DropTable("dbo.CharacterTitles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CharacterTitles",
                c => new
                    {
                        TitleKey = c.Long(nullable: false, identity: true),
                        CharacterId = c.Long(nullable: false),
                        TitleId = c.Int(nullable: false),
                        Stat = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.TitleKey);
            
            AddColumn("dbo.CharacterSkill", "IsPartnerSkill", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.CharacterTitle", "CharacterId", "dbo.Character");
            DropIndex("dbo.CharacterTitle", new[] { "CharacterId" });
            DropTable("dbo.CharacterTitle");
            CreateIndex("dbo.CharacterTitles", "CharacterId");
            AddForeignKey("dbo.CharacterTitles", "CharacterId", "dbo.Character", "CharacterId", cascadeDelete: true);
        }
    }
}
